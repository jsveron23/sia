import {
  REQUEST_FETCH_IMAGE,
  SUCCESS_FETCH_IMAGE,
  FAILURE_FETCH_IMAGE,
} from '../actionTypes';

export function fetchImage(id) {
  return async (dispatch, getState, api) => {
    dispatch({
      type: REQUEST_FETCH_IMAGE,
    });

    try {
      const { url } = await api.fetchImage(id);

      dispatch({
        type: SUCCESS_FETCH_IMAGE,
        payload: url,
      });
    } catch (err) {
      dispatch({
        type: FAILURE_FETCH_IMAGE,
        payload: err,
      });
    }
  };
}
