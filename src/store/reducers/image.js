import {
  REQUEST_FETCH_IMAGE,
  SUCCESS_FETCH_IMAGE,
  FAILURE_FETCH_IMAGE,
} from '../actionTypes';

const initialState = {
  isLoading: false,
  src: '',
  error: null,
};

function reducer(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case REQUEST_FETCH_IMAGE: {
      return {
        ...state,
        isLoading: true,
        src: '',
        error: null,
      };
    }

    case SUCCESS_FETCH_IMAGE: {
      return {
        ...state,
        isLoading: false,
        src: payload,
        error: null,
      };
    }

    case FAILURE_FETCH_IMAGE: {
      return {
        ...state,
        isLoading: false,
        src: '',
        error: payload,
      };
    }

    default: {
      return state;
    }
  }
}

export default reducer;
