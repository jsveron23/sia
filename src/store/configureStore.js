import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as api from 'services/api';
import reducers from './reducers';

function configureStore(preloadedState) {
  const isDev = process.env.NODE_ENV === 'development';
  const middlewares = [thunk.withExtraArgument(api)];
  const middlewareEnhancer = applyMiddleware(...middlewares);
  const composedEnhancer = isDev
    ? composeWithDevTools(middlewareEnhancer)
    : middlewareEnhancer;

  return createStore(reducers, preloadedState, composedEnhancer);
}

export default configureStore;
