import req from './instance';

export function fetchImage(id = 1) {
  return req.get(`/photos/${id}`);
}
