import { createRequest } from 'utils';

export default createRequest({
  options: {
    baseURL: 'https://jsonplaceholder.typicode.com',
  },
  onSuccess: ({ data }) => data,
  onError: (err) => console.error(err),
});
