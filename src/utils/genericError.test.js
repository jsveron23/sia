import genericError from './genericError';

test('should create error with message', () => {
  const err = genericError('this is a error');

  expect(err).toBeInstanceOf(Error);
  expect(err).toHaveProperty('message', 'this is a error');
});

test('should create error with specific props', () => {
  const err = genericError('this is a error', {
    a: 'this is a',
    b: 'this is b',
  });

  expect(err).toHaveProperty('a', 'this is a');
  expect(err).toHaveProperty('b', 'this is b');
});
