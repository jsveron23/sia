import calXY from './calXY';

const evt = {
  currentTarget: {
    getBoundingClientRect: () => ({
      left: 1,
      top: 1,
    }),
  },
  pageX: 1,
  pageY: 1,
};

test('should calculate x, y by event object', () => {
  const xy = calXY(evt);

  expect(xy).toHaveProperty('x', 0);
  expect(xy).toHaveProperty('y', 0);
});
