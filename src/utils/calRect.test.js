import calRect from './calRect';

test('should calculate w, h by provided object', () => {
  const drawing = calRect({
    startX: 1,
    startY: 2,
    x: 3,
    y: 4,
  });

  expect(drawing).toHaveProperty('w', 2);
  expect(drawing).toHaveProperty('h', 2);
});
