function calXY(evt) {
  const { left, top } = evt.currentTarget.getBoundingClientRect();

  return {
    x: evt.pageX - left,
    y: evt.pageY - top,
  };
}

export default calXY;
