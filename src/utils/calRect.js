function calRect(drawing) {
  return {
    x: drawing.startX,
    y: drawing.startY,
    w: drawing.x - drawing.startX,
    h: drawing.y - drawing.startY,
  };
}

export default calRect;
