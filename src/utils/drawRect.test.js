import drawRect from './drawRect';

const ctx = {};

beforeEach(() => {
  ctx.strokeRect = jest.fn();
});

afterEach(() => {
  jest.restoreAllMocks();
});

test('should drawRect is curry function', () => {
  const fn = drawRect(ctx);

  expect(fn).toBeInstanceOf(Function);
});

test('should use provided rect for calling strokeRect method', () => {
  drawRect(ctx)({
    x: 1,
    y: 1,
    w: 1,
    h: 1,
  });

  expect(ctx.strokeRect).toHaveBeenCalled();
  expect(ctx.strokeRect.mock.calls[0][0]).toHaveProperty('x', 1);
  expect(ctx.strokeRect.mock.calls[0][0]).toHaveProperty('y', 1);
  expect(ctx.strokeRect.mock.calls[0][0]).toHaveProperty('w', 1);
  expect(ctx.strokeRect.mock.calls[0][0]).toHaveProperty('h', 1);
});
