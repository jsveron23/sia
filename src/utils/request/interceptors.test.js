import axios from 'axios';
import interceptors from './interceptors';

jest.mock('axios', () => {
  const originalAxios = jest.requireActual('axios');

  return {
    __esModule: true,
    default: {
      ...originalAxios,
      interceptors: {
        response: { use: jest.fn(() => {}) },
      },
    },
  };
});

const config = {
  onSuccess: jest.fn(),
  onCancel: jest.fn(),
  onTimeout: jest.fn(),
  onError: jest.fn(),
};

test('should call onSuccess callback as provided', () => {
  const instance = interceptors(config)(axios);
  const responseCallback = instance.interceptors.response.use.mock.calls[0][0];
  responseCallback();

  expect(config.onSuccess).toHaveBeenCalled();
});

test('should call onError callback as provided', () => {
  const instance = interceptors(config)(axios);
  const rejectCallback = instance.interceptors.response.use.mock.calls[0][1];
  rejectCallback({
    response: {
      status: 500,
    },
  }).catch(() => {});

  expect(config.onError).toHaveBeenCalled();
});
