import { identity } from 'ramda';

function interceptors(config) {
  const { onSuccess = identity, onError = identity } = config;

  return (instance) => {
    instance.interceptors.response.use(
      (response) => {
        const data = onSuccess(response);

        return data || response;
      },

      (error) => {
        return Promise.reject(onError(error) || error);
      }
    );

    return instance;
  };
}

export default interceptors;
