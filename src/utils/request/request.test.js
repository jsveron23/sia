import axios from 'axios';
import createRequest from '.';

jest.mock('./interceptors', () => {
  return {
    __esModule: true,
    default: () => (v) => v,
  };
});

test('should create axios instance', () => {
  const instance = createRequest();

  expect(instance).toHaveProperty('post');
  expect(instance).toHaveProperty('get');
  expect(instance).toHaveProperty('put');
  expect(instance).toHaveProperty('patch');
  expect(instance).toHaveProperty('delete');
});

test('should call axios.create method', () => {
  const axiosCreate = jest.spyOn(axios, 'create');
  createRequest();

  expect(axiosCreate).toHaveBeenCalled();
});

test('should call axios.create method with provided options', () => {
  const axiosCreate = jest.spyOn(axios, 'create');
  const options = {
    timeout: 1000,
    baseURL: 'https://si-analytics.ai/',
  };

  createRequest({ options });

  expect(axiosCreate).toHaveBeenCalledWith(options);
});
