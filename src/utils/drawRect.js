function drawRect(ctx) {
  ctx.strokeStyle = 'rgb(86, 104, 217)';
  ctx.lineWidth = 1;

  return (...rect) => ctx.strokeRect(...rect);
}

export default drawRect;
