import calCenter from './calCenter';

test('should calculate center of label', () => {
  const center = calCenter(200, 200, 100, 100);

  expect(center).toHaveProperty('x', 150);
  expect(center).toHaveProperty('y', 150);
});
