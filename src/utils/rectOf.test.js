import rectOf from './rectOf';

test('should not return negative width, heght values', () => {
  const rect = rectOf({ x: 100, y: 100, w: -100, h: -100 });

  expect(rect).toHaveProperty('w', 100);
  expect(rect).toHaveProperty('h', 100);
});

test('should return x, y are `x: x + width`, `y: y + height` so zero', () => {
  const rect = rectOf({ x: 100, y: 100, w: -100, h: -100 });

  expect(rect).toHaveProperty('x', 0);
  expect(rect).toHaveProperty('y', 0);
});

test('should return x, y are same as provided', () => {
  const rect = rectOf({ x: 100, y: 100, w: 100, h: 100 });

  expect(rect).toHaveProperty('x', 100);
  expect(rect).toHaveProperty('y', 100);
});
