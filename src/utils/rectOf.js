function rectOf(rect) {
  return {
    ...rect,
    x: rect.w > 0 ? rect.x : rect.w + rect.x,
    y: rect.h > 0 ? rect.y : rect.h + rect.y,
    w: Math.abs(rect.w),
    h: Math.abs(rect.h),
  };
}

export default rectOf;
