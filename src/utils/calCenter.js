function calCenter(x, y, w, h) {
  return {
    x: x - w / 2,
    y: y - h / 2,
  };
}

export default calCenter;
