import { AnnotationContainer } from 'containers';
import 'styles/app.css';

function App() {
  return <AnnotationContainer />;
}

export default App;
