import {
  SET_CONTEXT,
  SET_BOUND,
  SET_LABELS,
  UPDATE_LABELS,
  SET_SELECTED_LABEL,
  DELETE_SELECTED_LABEL,
  SET_DRAGGING_LABEL_ID,
  SET_MODE,
} from './internalActionTypes';

export const initialState = {
  ctx: null,
  bound: null,
  mode: 'creation', // selection, creation
  labels: [],
  draggingLabelId: '',
};

export function reducer(state, action) {
  const { type, payload } = action;

  switch (type) {
    case SET_CONTEXT: {
      return {
        ...state,
        ctx: payload,
      };
    }

    case SET_BOUND: {
      return {
        ...state,
        bound: payload,
      };
    }

    case SET_LABELS: {
      const labels = [...state.labels, payload];

      return {
        ...state,
        labels: labels.filter(Boolean),
      };
    }

    case SET_SELECTED_LABEL: {
      return {
        ...state,
        labels: state.labels.map((l) => {
          if (l.selected) {
            return {
              ...l,
              selected: payload !== l.id,
            };
          }

          return {
            ...l,
            selected: payload === l.id,
          };
        }),
      };
    }

    case DELETE_SELECTED_LABEL: {
      return {
        ...state,
        labels: state.labels.filter((l) => {
          return payload.indexOf(l.id) === -1;
        }),
      };
    }

    case SET_DRAGGING_LABEL_ID: {
      return {
        ...state,
        draggingLabelId: payload,
      };
    }

    case UPDATE_LABELS: {
      const labels = state.labels.map((l) => {
        if (payload.id !== l.id) {
          return l;
        }

        return {
          ...l,
          rect: {
            ...l.rect,
            x: payload.x,
            y: payload.y,
          },
        };
      });

      return {
        ...state,
        labels,
      };
    }

    case SET_MODE: {
      return {
        ...state,
        mode: payload,
      };
    }

    default: {
      return state;
    }
  }
}
