import { useEffect, useReducer, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { identity } from 'ramda';
import { Viewport, Image, Board, Canvas, Labels } from 'components';
import ToolbarSelectIcon from 'assets/svg/Toolbar_select.svg';
import BoundingBoxCreateIcon from 'assets/svg/Bounding_Box_Create.svg';
import * as types from './internalActionTypes';
import { initialState, reducer } from './internalReducer';

function AnnotationPage({ isLoading, src, sid, fetchImage, handlers }) {
  const [img, setImg] = useState(null);
  const [canvas, setCanvas] = useState(null);
  const [state, dispatch] = useReducer(reducer, initialState);
  const isCreation = state.mode === 'creation';

  /** @see containers */
  const actions = handlers(state, dispatch, types, { img, canvas });

  useEffect(() => {
    fetchImage(sid);
  }, [sid, fetchImage]);

  useEffect(() => {
    if (canvas && img) {
      const ctx = canvas.getContext('2d');

      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);

      dispatch({
        type: types.SET_CONTEXT,
        payload: ctx,
      });
    }

    if (canvas) {
      dispatch({
        type: types.SET_BOUND,
        payload: canvas.getBoundingClientRect(),
      });
    }
  }, [canvas, img]);

  const sidebarBtns = useMemo(
    () => [
      {
        mode: 'selection',
        src: ToolbarSelectIcon,
        isActivated: state.mode,
        onClick: () => {
          dispatch({
            type: types.SET_MODE,
            payload: 'selection',
          });
        },
      },
      {
        mode: 'creation',
        src: BoundingBoxCreateIcon,
        isActivated: state.mode,
        onClick: () => {
          dispatch({
            type: types.SET_MODE,
            payload: 'creation',
          });
        },
      },
    ],
    [state.mode]
  );

  if (isLoading) {
    return (
      <Viewport>
        <Board>Loading image...</Board>
      </Viewport>
    );
  }

  return (
    <Viewport
      onKeyDown={actions.handleKeyDown}
      sidebarBtns={sidebarBtns}
      tabIndex="0"
    >
      <Board>
        <Canvas
          ref={setCanvas}
          onMouseDown={(evt) => {
            if (isCreation) {
              actions.handleMouseDown(evt);
            }
          }}
          onMouseMove={(evt) => {
            if (isCreation) {
              actions.handleMouseMove(evt);
            }
          }}
          onMouseUp={(evt) => {
            if (isCreation) {
              actions.handleMouseUp(evt);
            }
          }}
        />
        <Image src={src} ref={setImg} display="none" />

        <Labels
          labels={state.labels}
          bound={state.bound}
          mode={state.mode}
          draggingLabelId={state.draggingLabelId}
          setDraggingLabelId={actions.setDraggingLabelId}
          onClick={actions.handleSelectLabel}
          updateLabels={actions.updateLabels}
        />
      </Board>
    </Viewport>
  );
}

AnnotationPage.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired,
  fetchImage: PropTypes.func.isRequired,
  sid: PropTypes.number,
  handlers: PropTypes.func,
};

AnnotationPage.defaultProps = {
  sid: 1,
  handlers: () => identity,
};

export default AnnotationPage;
