import { memo } from 'react';
import PropTypes from 'prop-types';
import Box from 'ui-box';

function Board({ children, ...props }) {
  return (
    <Box paddingTop={20} paddingLeft={80} {...props}>
      {children}
    </Box>
  );
}

Board.propTypes = {
  children: PropTypes.node.isRequired,
};

export default memo(Board);
