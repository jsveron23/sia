import { memo, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { identity } from 'ramda';
import Box from 'ui-box';

const Image = forwardRef(function Image({ src, alt, onLoad, ...props }, ref) {
  return (
    <Box is="img" src={src} alt={alt} ref={ref} onLoad={onLoad} {...props} />
  );
});

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  onLoad: PropTypes.func,
};

Image.defaultProps = {
  alt: '',
  onLoad: identity,
};

export default memo(Image);
