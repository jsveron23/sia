import { memo } from 'react';
import PropTypes from 'prop-types';
import Box from 'ui-box';

function TitleBar({ children }) {
  return (
    <Box
      width="100%"
      height={64}
      background="#FCFCFC 0% 0% no-repeat padding-box"
      border="1px solid #EBEDF2"
      borderTop="none"
      paddingLeft={56}
      paddingTop={21}
      paddingBottom={16}
    >
      <Box
        is="p"
        font="normal normal 600 20px/27px Noto Sans Display"
        color="#141746"
      >
        {children}
      </Box>
    </Box>
  );
}

TitleBar.propTypes = {
  children: PropTypes.node.isRequired,
};

export default memo(TitleBar);
