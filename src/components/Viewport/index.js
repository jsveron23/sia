import { memo } from 'react';
import PropTypes from 'prop-types';
import { identity } from 'ramda';
import Box from 'ui-box';
import Header from '../Header';
import TitleBar from '../TitleBar';
import Sidebar from '../Sidebar';

function Viewport({ children, sidebarBtns, onKeyDown, ...props }) {
  return (
    <Box width="100vw" height="100vh" onKeyDown={onKeyDown} {...props}>
      <Header />
      <TitleBar>Dataset Label</TitleBar>

      <Sidebar btns={sidebarBtns} />

      {children}
    </Box>
  );
}

Viewport.propTypes = {
  children: PropTypes.node.isRequired,
  sidebarBtns: PropTypes.arrayOf(
    PropTypes.shape({
      mode: PropTypes.string,
      src: PropTypes.string,
      isActivated: PropTypes.string,
      onClick: PropTypes.func,
    })
  ),
  onKeyDown: PropTypes.func,
};

Viewport.defaultProps = {
  onClick: identity,
};

export default memo(Viewport);
