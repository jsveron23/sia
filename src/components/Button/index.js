import { memo, forwardRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { identity } from 'ramda';
import Box from 'ui-box';

const Button = forwardRef(function Button(
  { className, onClick, ...props },
  ref
) {
  return (
    <Box
      is="button"
      className={cx('btn', className)}
      ref={ref}
      onClick={onClick}
      {...props}
    />
  );
});

Button.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  className: '',
  onClick: identity,
};

export default memo(Button);
