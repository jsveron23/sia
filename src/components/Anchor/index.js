import { memo, forwardRef } from 'react';
// import PropTypes from 'prop-types';
import Box from 'ui-box';

const Anchor = forwardRef(function Anchor(props, ref) {
  return (
    <Box className="anchor">
      <Box className="anchor-pillar" />
      <Box className="anchor-box left-top-center-round" />

      <Box className="anchor-box left-top" />
      <Box className="anchor-box right-top" />
      <Box className="anchor-box left-bottom" />
      <Box className="anchor-box right-bottom" />

      <Box className="anchor-box left-middle" />
      <Box className="anchor-box right-middle" />
      <Box className="anchor-box top-center" />
      <Box className="anchor-box bottom-center" />
    </Box>
  );
});

// Anchor.propTypes = {};
//
// Anchor.defaultProps = {};

export default memo(Anchor);
