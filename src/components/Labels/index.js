import { memo } from 'react';
import PropTypes from 'prop-types';
import { identity } from 'ramda';
import Label from '../Label';

function Labels({
  labels,
  bound,
  mode,
  draggingLabelId,
  onClick,
  setDraggingLabelId,
  updateLabels,
}) {
  return labels.map(({ rect, tag, self, id, selected }) => {
    const styleProps = {
      left: bound.left + rect.x,
      top: bound.top + rect.y,
      width: rect.w,
      height: `${rect.h}px`,
      className: 'annotation-box',
    };

    return (
      <Label
        key={id}
        id={id}
        mode={mode}
        rect={self.rect}
        draggingLabelId={draggingLabelId}
        onClick={onClick}
        setDraggingLabelId={setDraggingLabelId}
        updateLabels={updateLabels}
        {...styleProps}
      >
        {tag}
      </Label>
    );
  });
}

Labels.propTypes = {
  labels: PropTypes.arrayOf(
    PropTypes.shape({
      rect: PropTypes.shape({
        x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
        y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
      }),
    })
  ).isRequired,
  mode: PropTypes.string.isRequired,
  bound: PropTypes.instanceOf(DOMRect),
  draggingLabelId: PropTypes.string,
  onClick: PropTypes.func,
  setDraggingLabelId: PropTypes.func,
  updateLabels: PropTypes.func,
};

Labels.defaultProps = {
  labels: [],
  bound: null,
  draggingLabelId: '',
  onClick: identity,
  setDraggingLabelId: identity,
  updateLabels: identity,
};

export default memo(Labels);
