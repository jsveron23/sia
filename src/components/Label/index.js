import { memo, useRef, useCallback } from 'react';
import PropTypes from 'prop-types';
import { identity } from 'ramda';
import Box from 'ui-box';
import { calCenter } from 'utils';
import Anchor from '../Anchor';

function Label({
  id,
  rect,
  mode,
  actions,
  children,
  draggingLabelId,
  onClick,
  setDraggingLabelId,
  updateLabels,
  ...props
}) {
  const isClicked = useRef(false);
  const label = useRef(null);
  const isSelection = mode === 'selection';
  const isReadyToMove = isClicked.current && isSelection;

  const handleClick = useCallback(() => {
    if (isSelection) {
      onClick(id);
      isClicked.current = !isClicked.current;
    }
  }, [id, isSelection, onClick]);

  const handleMouseDown = useCallback(() => {
    if (isReadyToMove) {
      setDraggingLabelId(id);
    }
  }, [isReadyToMove, id, setDraggingLabelId]);

  const handleMouseMove = useCallback(
    (evt) => {
      if (isReadyToMove && draggingLabelId) {
        const { x, y } = calCenter(evt.pageX, evt.pageY, rect.w, rect.h);

        label.current.style.cssText = `
          left: ${x}px;
          top: ${y}px;
        `;
      }
    },
    [isReadyToMove, draggingLabelId, rect.w, rect.h]
  );

  const handleMouseUp = useCallback(() => {
    if (isReadyToMove) {
      isClicked.current = true;
      setDraggingLabelId('');
      updateLabels(id, label.current.style.left, label.current.style.top);
    }
  }, [isReadyToMove, setDraggingLabelId, updateLabels, id]);

  return (
    <Box
      onClick={handleClick}
      onMouseDown={handleMouseDown}
      onMouseMove={handleMouseMove}
      onMouseUp={handleMouseUp}
      ref={label}
      {...props}
    >
      {isClicked.current && <Anchor />}

      <Box padding={10}>{children}</Box>
    </Box>
  );
}

Label.propTypes = {
  id: PropTypes.string.isRequired,
  rect: PropTypes.shape({
    x: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    y: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  }).isRequired,
  mode: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  draggingLabelId: PropTypes.string,
  setDraggingLabelId: PropTypes.func,
  updateLabels: PropTypes.func,
  onClick: PropTypes.func,
};

Label.defaultProps = {
  draggingLabelId: '',
  setDraggingLabelId: identity,
  updateLabels: identity,
  onClick: identity,
};

export default memo(Label);
