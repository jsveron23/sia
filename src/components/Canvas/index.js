import { memo, forwardRef } from 'react';
// import PropTypes from 'prop-types';
import Box from 'ui-box';

const Canvas = forwardRef(function Canvas(props, ref) {
  return <Box is="canvas" ref={ref} {...props} />;
});

// Canvas.propTypes = {};
//
// Canvas.defaultProps = {};

export default memo(Canvas);
