import { memo } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Box from 'ui-box';
import Image from '../Image';
import Button from '../Button';

function Sidebar({ btns }) {
  return (
    <Box
      is="section"
      position="fixed"
      width={56}
      height="100%"
      background="#FCFCFC 0% 0% no-repeat padding-box"
      border="1px solid #EBEDF2"
      borderTop="none"
      padding={8}
    >
      <Box
        width="100%"
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        {btns.map(({ mode, src, isActivated, onClick }) => {
          return (
            <Button
              key={mode}
              className={cx({
                'is-activated': mode === isActivated,
              })}
              width={40}
              height={40}
              onClick={onClick}
            >
              <Image src={src} width="100%" height="100%" />
            </Button>
          );
        })}
      </Box>
    </Box>
  );
}

Sidebar.propTypes = {
  btns: PropTypes.arrayOf(
    PropTypes.shape({
      mode: PropTypes.string,
      src: PropTypes.string,
      isActivated: PropTypes.string,
      onClick: PropTypes.func,
    })
  ),
};

Sidebar.defaultProps = {
  btns: [],
};

export default memo(Sidebar);
