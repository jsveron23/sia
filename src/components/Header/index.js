import { memo } from 'react';
import Box from 'ui-box';
import Image from '../Image';
import minimizeIcon from 'assets/svg/minimize.svg';
import smallerIcon from 'assets/svg/smaller.svg';
import quitIcon from 'assets/svg/Quit.svg';

function Header() {
  return (
    <Box
      is="header"
      width="100%"
      height={40}
      background="#EBEDF2 0% 0% no-repeat padding-box"
      border="1px solid #D5D9E2"
      borderTop="none"
    >
      <Box display="flex" justifyContent="flex-end">
        <Image src={minimizeIcon} />
        <Image src={smallerIcon} />
        <Image src={quitIcon} />
      </Box>
    </Box>
  );
}

export default memo(Header);
