import { connect } from 'react-redux';
import { compose, values, apply } from 'ramda';
import hash from 'object-hash';
import { calXY, calRect, drawRect, rectOf } from 'utils';
import { AnnotationPage } from 'pages';
import { fetchImage } from 'store/actions';

const KEY_CODE = {
  Delete: 8,
};

const drawing = {
  x: -1,
  y: -1,
  startX: -1,
  startY: -1,
};
let isDrawing = false;
let rAF = null;

function mapStateToProps({ image: { isLoading, src } }) {
  return {
    sid: 1,
    isLoading,
    src,

    // NOTE too many functions in the component
    handlers(state, dispatch, types, refs) {
      const updateCanvas = () => {
        state.ctx.drawImage(
          refs.img,
          0,
          0,
          refs.canvas.width,
          refs.canvas.height
        );

        if (isDrawing) {
          rAF = requestAnimationFrame(updateCanvas);

          compose(apply(drawRect(state.ctx)), values, calRect)(drawing);
        }
      };

      return {
        handleKeyDown(evt) {
          const isSelection = state.mode === 'selection';

          if (isSelection && evt.keyCode === KEY_CODE.Delete) {
            dispatch({
              type: types.DELETE_SELECTED_LABEL,
              payload: state.labels.filter((l) => l.selected).map((l) => l.id),
            });
          }
        },

        handleMouseDown(evt) {
          isDrawing = true;

          cancelAnimationFrame(rAF);
          rAF = requestAnimationFrame(updateCanvas);

          const { x, y } = calXY(evt);

          drawing.x = x;
          drawing.y = y;
          drawing.startX = x;
          drawing.startY = y;
        },

        handleMouseMove(evt) {
          const { x, y } = calXY(evt);

          drawing.x = x;
          drawing.y = y;
        },

        handleMouseUp() {
          isDrawing = false;

          const tag = prompt('Enter the name');

          if (tag) {
            const rect = compose(rectOf, calRect)(drawing);
            const labelProps = {
              rect,
              tag,
            };

            dispatch({
              type: types.SET_LABELS,
              payload: {
                id: hash(labelProps),
                self: labelProps,
                selected: false,
                ...labelProps,
              },
            });
          }

          dispatch({
            type: types.SET_LABELS,
            payload: null,
          });
        },

        handleSelectLabel(id) {
          dispatch({
            type: types.SET_SELECTED_LABEL,
            payload: id,
          });
        },

        setDraggingLabelId(v) {
          dispatch({
            type: types.SET_DRAGGING_LABEL_ID,
            payload: v,
          });
        },

        updateLabels(id, x, y) {
          dispatch({
            type: types.UPDATE_LABELS,
            payload: {
              id,
              x,
              y,
            },
          });
        },
      };
    },
  };
}

export default connect(mapStateToProps, { fetchImage })(AnnotationPage);
