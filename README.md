# sia-assignment

## Install

```
$ npm ci
$ npm start
```

## Tests

```
$ npm test
```

## Structure

* assets - asset files
* components - dumb components
* contaners - data components
* pages - page layout components
* services - cummnicate with (api, localstorage)
* store - Redux
* styles - CSS (SMACK)
* utils - utilities
